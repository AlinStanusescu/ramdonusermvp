//
//  UserDetailRowModel.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 28.10.2021.
//

import Foundation

class UserDetailRowModel: NSObject {

    let title: String
    
    init(title: String) {
        self.title = title
    }
}
