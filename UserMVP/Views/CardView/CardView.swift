//
//  CardView.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import UIKit

class CardView: NibLoadingView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardAdress: UILabel!
    @IBOutlet weak var cardNationality: UILabel!
    
    typealias TapHandler = (_ sender: CardView) -> Void
    var tapHandler: TapHandler?
    
    var model: CardModel? {
        didSet {
            self.reloadView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView = legacyLoadXib()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        contentView = legacyLoadXib()
        setup()
    }
    
    func reloadView() {
        guard let model = model else {
            return
        }
                
        let url = URL(string: model.userPreviewImage)
        DispatchQueue.main.async {
            let data = try? Data(contentsOf: url!) 
            self.imageView.image = UIImage(data: data!)
        }
    
        cardName.text =  model.userName
        cardAdress.text = model.state 
        cardNationality.text = model.nationality 
    }
    
    override func setup() {
        super.setup()
        
        cardName.font = UIFont.systemFont(ofSize: 15)
        cardName.textColor = Colors.darkGrey3
        
        cardAdress.font = UIFont.systemFont(ofSize: 13)
        cardAdress.textColor = Colors.textColor_MildGrey1
                
        cardNationality.font =  UIFont.systemFont(ofSize: 13)
        cardNationality.textColor = Colors.textColor_MildGrey2
        
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    // Mark: - IBAction
    @IBAction func viewTapped(sender: Any?) {
        self.tapHandler?(self)
    }
}

