//
//  CardModel.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import Foundation

class CardModel: NSObject {
    
    var model: Result
    
    init(model: Result) {
        self.model = model
    }
    
    var userPreviewImage: String {
        return model.picture?.thumbnail ?? ""
    }
    
    var userName: String {
        return "\(model.name?.title ??  ""). \(model.name?.first  ?? "") \(model.name?.last ?? "")"
    }
    
    var nationality: String  {
        return  model.nat  ?? ""
    }
    
    var state: String {
        return "\(model.location?.state ?? "") | \(model.location?.country ?? "") | \(model.location?.city ?? "")"
    }
}

