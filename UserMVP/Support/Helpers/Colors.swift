//
//  Colors.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import UIKit

struct Colors {
    
    static let textColor_MildGrey1 = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1.0) // #999999
    static let textColor_MildGrey2 = UIColor(red: 102.0/255.0, green: 102.0/255.0, blue: 102.0/255.0, alpha: 1.0) // #666666
    static let darkGrey3            = UIColor(red: 35/255, green: 35/255, blue: 35/255, alpha: 1.0) // #232323;
}
