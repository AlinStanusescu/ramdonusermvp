//
//  UIView+Nib.swift
//  -
//
//  Created by Alin Stanusescu on 04/06/2018.
//  Copyright © 2018 Orange. All rights reserved.
//
//  -
//
//  Created by Alin Stanusescu on 04/06/2018.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

internal func cast<T, U>(_ value: T) -> U? {
    return value as? U
}

internal extension UIView {
    
    class func loadXib(owner: Any? = nil) -> Self {
        let xibName = String(describing: type(of: self)).components(separatedBy: ".")[0]
        let view = UINib(nibName: xibName, bundle: Bundle.main).instantiate(withOwner: owner, options: nil)[0]
        return cast(view)!
    }
    
    func loadCusotmXib() -> Self {
        
        let shouldLoad = (subviews.count == 0)
        if shouldLoad {
            let view = type(of: self).loadXib()
            view.frame = frame
            view.translatesAutoresizingMaskIntoConstraints = false
            
            for constraint in constraints {
                let newConstraint = NSLayoutConstraint(item: constraint.firstItem === self ? view : constraint.firstItem ?? view,
                                                       attribute: constraint.firstAttribute,
                                                       relatedBy: constraint.relation,
                                                       toItem: constraint.secondItem === self ? view : constraint.secondItem,
                                                       attribute: constraint.secondAttribute,
                                                       multiplier: 1,
                                                       constant: constraint.constant)
                view.addConstraint(newConstraint)
            }
            (self as UIView).removeConstraints(constraints)
            return view
        }
        return self
    }
    
    @discardableResult
    func legacyLoadXib<T : UIView>() -> T? {
        let nibName = String(describing: type(of: self))
        let nibNameWithoutGenericType = nibName.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        guard let view = Bundle.main.loadNibNamed(nibNameWithoutGenericType, owner: self, options: nil)?[0] as? T else {
            return nil
        }
        self.addSubview(view)
        view.addEdgeConstraints()
        return view
    }
    
    static func inflate<T>(withOwner: Any? = nil, options: [AnyHashable : Any]? = nil) -> T where T: UIView {
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: "\(self)", bundle: bundle)
        
        guard let view = nib.instantiate(withOwner: withOwner, options: options as? [UINib.OptionsKey : Any]).first as? T else {
            fatalError("Could not load view from nib file.")
        }
        return view
    }
    
}

extension UIView {
    
    func setHeight(_ value: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalToConstant: value).isActive = true
    }
    
    func setWidth(_ value: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.widthAnchor.constraint(equalToConstant: value).isActive = true
    }
    
    func addEdgeConstraints() {
        guard let superview = self.superview else {
            return
        }
        self.translatesAutoresizingMaskIntoConstraints = false
        superview.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        superview.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        superview.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        superview.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func attach(to containerView: UIView) {
        
        #if DEBUG
        guard Thread.isMainThread == true else {
            assertionFailure("Check caller thread")
            return
        }
        #endif
        
        if self.superview != nil {
            self.removeFromSuperview()
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addSubview(self)
        
        self.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        self.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }
    
}
