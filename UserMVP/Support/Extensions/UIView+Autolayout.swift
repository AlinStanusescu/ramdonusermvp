//
//  UIView+Autolayout.swift
//  -
//
//  Created by Alin Stanusescu on 04/06/2018.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

extension UIView {
    func addAllMarginConstraint(constant : CGFloat = 0.0){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addMarginConstraint(.top, constant: constant)
        self.addMarginConstraint(.bottom, constant: constant)
        self.addMarginConstraint(.leading, constant: constant)
        self.addMarginConstraint(.trailing, constant: constant)
    }
    
    @discardableResult
    func addMarginConstraint(_ attribute: NSLayoutConstraint.Attribute, constant: CGFloat, priority: Int = 1000) -> NSLayoutConstraint? {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            let constraint = NSLayoutConstraint(item: self,
                                                attribute: attribute,
                                                relatedBy: .equal,
                                                toItem: superview,
                                                attribute: attribute,
                                                multiplier: 1,
                                                constant: constant)
            constraint.priority = UILayoutPriority(rawValue: UILayoutPriority.RawValue(priority))
            superview.addConstraint(constraint)
            return constraint
        }
        return nil
    }
    
    @discardableResult
    func addMarginConstraint(_ attribute: NSLayoutConstraint.Attribute) -> NSLayoutConstraint? {
        return addMarginConstraint(attribute, constant: 0)
    }
    
    @discardableResult
    func addAllMarginConstraints() -> Array<NSLayoutConstraint> {
        var constraints = Array<NSLayoutConstraint>.init()
        for attribute in [NSLayoutConstraint.Attribute.left, NSLayoutConstraint.Attribute.right, NSLayoutConstraint.Attribute.top, NSLayoutConstraint.Attribute.bottom, NSLayoutConstraint.Attribute.leading, NSLayoutConstraint.Attribute.trailing] {
            if let constraint = addMarginConstraint(attribute) {
                constraints.append(constraint)
            }
        }
        return constraints
    }
    
    @discardableResult
    func addHeightConstraint(_ constant: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: NSLayoutConstraint.Attribute.height,
                                            relatedBy: .equal,
                                            toItem: nil,
                                            attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                                            multiplier: 1.0,
                                            constant: constant)
        self.addConstraint(constraint)
        return constraint
    }
    
    @discardableResult
    func addWidthConstraint(_ constant: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self,
                                            attribute: NSLayoutConstraint.Attribute.width,
                                            relatedBy: .equal,
                                            toItem: nil,
                                            attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                                            multiplier: 1.0,
                                            constant: constant)
        self.addConstraint(constraint)
        return constraint
    }

    /// attaches the trailing edge of the current view to the given view
    @discardableResult
    func layoutAttachTrailing(to: UIView? = nil, margin : CGFloat = 0.0, priority: UILayoutPriority? = nil) -> NSLayoutConstraint {

      let view: UIView? = to ?? superview
      let isSuperview = (view == superview) || false
      let constraint = NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: isSuperview ? .trailing : .leading, multiplier: 1.0, constant: -margin)
      if let priority = priority {
        constraint.priority = priority
      }
      superview?.addConstraint(constraint)

      return constraint
    }

    /// attaches the leading edge of the current view to the given view
    @discardableResult
    func layoutAttachLeading(to: UIView? = nil, margin : CGFloat = 0.0) -> NSLayoutConstraint {

      let view: UIView? = to ?? superview
      let isSuperview = (view == superview) || false
      let constraint = NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: view, attribute: isSuperview ? .leading : .trailing, multiplier: 1.0, constant: margin)
      superview?.addConstraint(constraint)

      return constraint
    }

    /// attaches height constraint to the given view
    @discardableResult
    func layoutAttachHeight(height : CGFloat = 0.0, priority: UILayoutPriority? = nil) -> NSLayoutConstraint {

      let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute:.notAnAttribute, multiplier: 1.0, constant: height)
      if let priority = priority {
        constraint.priority = priority
      }
      self.addConstraint(constraint)

      return constraint
    }
}
