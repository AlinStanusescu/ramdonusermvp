// Mihai Cristian Tanase ©2019 Orange Money. All rights reserved.
//
//  NibLoadingView.swift
//  -
//
//  Created by Alin Stanusescu on 04/06/2018.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class NibLoadingView: UIView {
    var contentView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func setup() {
        contentView = loadNib()
    }
}
