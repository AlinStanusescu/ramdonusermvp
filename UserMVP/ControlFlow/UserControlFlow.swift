//
//  UserControlFlow.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import UIKit

class UserControlFlow: NSObject {
    
    static let shared = UserControlFlow()
    
    private override init() { }
}

protocol UserControlFlowNavigation {
    
    func backAction(parentController: UIViewController?)
        
    func presentUserCardDetails(_ parentController: UIViewController?, model: Result)
}


extension UserControlFlow: UserControlFlowNavigation {
    
    func backAction(parentController: UIViewController?) {
        if let navigationController = parentController?.navigationController {
            if navigationController.viewControllers.count > 1 {
                navigationController.popViewController(animated: true)
            } else {
                navigationController.dismiss(animated: true, completion: nil)
            }
        } else {
            parentController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func presentUserCardDetails(_ parentController: UIViewController?, model: Result) {
        let controller = UserCardDetailsViewController.createController(model: model)
        parentController?.present(controller, animated: true, completion: nil)
    }
    
}
