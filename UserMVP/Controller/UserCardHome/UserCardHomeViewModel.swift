//
//  UserCardHomeViewModel.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import Foundation

class UserCardHomeViewModel: NSObject {
    
    var userModel: User?
    
    func fetchUsers(successHandler: @escaping (User?) -> Void,
                                errorHandler: @escaping (Error) -> Void) {
        
        APIClient.fetchRandomUsers(pageResultstNo: Constants.noOfRequestedUsers) { result in
            guard let user = result else {
                successHandler(nil)
                return
            }
            
            self.userModel = user
            self.persistUserData(user)
            successHandler(user)
        }  errorHandler: { error in
            self.fetchUserDataFromLocal()
            errorHandler(error)
        }
    }
    
    private func persistUserData(_ user: User) {
        UDManagerClient.removeUserModel()
        UDManagerClient.saveUserModel(user: user)
    }
    
    private func fetchUserDataFromLocal() {
        UDManagerClient.fetchUserModel()
        self.userModel = UDManagerClient.userModel
    }
}
