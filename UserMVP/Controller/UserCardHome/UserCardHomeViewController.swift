//
//  UserCardHomeViewController.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import UIKit
import JGProgressHUD

class UserCardHomeViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var cardStack: UIStackView!
    
    var viewModel = UserCardHomeViewModel()
    let loadingAnimationDuration: TimeInterval = 0.3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.loadData(firstLoad: true)
    }
    
    private func setup() {
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        self.titleLabel.textColor = Colors.darkGrey3
        self.titleLabel.text = "Lista de utilizatori din diferite regiuni/zone ale lumii"
        
        self.subtitleLabel.font = UIFont.systemFont(ofSize: 13)
        self.subtitleLabel.textColor = Colors.textColor_MildGrey2
        self.subtitleLabel.text = "*pentru mai multe detalii tap pe un utilizator"
    }
    
    private func loadData(firstLoad: Bool = false) {
        let hud = JGProgressHUD()
        self.presentLoadingActivityView(hud: hud)
        
        self.viewModel.fetchUsers{ [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.removeLoadingActivityView(hud: hud)
                if result != nil {
                    self.setupView()
                    if firstLoad {
                        self.animateInLoadedData()
                    }
                }
            }
        } errorHandler: { error in
            self.removeLoadingActivityView(hud: hud)
            self.presetErrorMessageAlert()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                self?.clearStackView()
                self?.loadData(firstLoad: true)
            }
        }
    }
    
    // MARK:-
    private func setupView() {
        guard let dataSource = self.viewModel.userModel?.results else {
            return
        }
        
        for model in dataSource {
            let view = CardView.init()
            view.model = CardModel.init(model: model)
            view.tapHandler = self.viewTapped(_:)
            cardStack.addArrangedSubview(view)
        }
        cardStack.layoutIfNeeded()
    }
    
    private func clearStackView() {
        self.cardStack.subviews.forEach { $0.removeFromSuperview() }
    }
    
    private func animateInLoadedData() {
        self.cardStack.alpha = 0
        self.cardStack.isHidden = false
        
        UIView.animate(withDuration: loadingAnimationDuration) {
            self.cardStack.alpha = 1
        }
    }
        
    // MARK:-
    private func viewTapped(_ sender: CardView) {
        guard let model = sender.model?.model else { return }
        self.presentUserCardDetailsScreen(model: model)
    }
    
    // MARK:-
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let  bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if (bottomEdge >= scrollView.contentSize.height) {
            self.loadData()
        }
    }
}
