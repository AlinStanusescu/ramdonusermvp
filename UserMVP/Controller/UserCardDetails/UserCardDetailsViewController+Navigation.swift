//
//  UserCardDetailsViewController+Navigation.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 28.10.2021.
//


import UIKit

extension UserCardDetailsViewController {
    
    /// Build controller
    class func createController(model: Result) -> UserCardDetailsViewController {
        let controller = UserCardDetailsViewController.create()
        controller.viewModel = UserCardDetailsModel(model: model)
        return controller
    }
}
