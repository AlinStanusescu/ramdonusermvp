//
//  UserCardDetailsModel.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 28.10.2021.
//

import Foundation

class UserCardDetailsModel: NSObject {
    
    var model: Result
    
    init(model: Result) {
        self.model = model
    }
    
    var userName: String {
        return "\(model.name?.title ??  ""). \(model.name?.first  ?? "") \(model.name?.last ?? "")"
    }

    var state: String {
        return "Stat: \(model.location?.state ?? ""), \(model.location?.country ?? ""), \(model.location?.city ?? "")"
    }
    
    var address: String {
        return "Adresa: \(model.location?.street?.number ?? 0), \(model.location?.street?.name ?? "")"
    }

    var timezone: String  {
        return "Time zone: \(model.location?.timezone?.offset ?? ""), \(model.location?.timezone?.timezoneDescription ?? "")"
    }
    
    var nationality: String  {
        return "Nationalitate: \(model.nat ?? "")"
    }
    
    var email: String {
        return "Adresa de email: \(model.email ?? "")"
    }
    
    var loginDetails: String {
        return "Username: \(model.login?.username ?? "")\nParola: \(model.login?.password ?? "")\nUUID: \(model.login?.uuid ?? "") "
    }
}
