//
//  UDManagerClient.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 28.10.2021.
//

import Foundation

class UDManagerClient: NSObject {
    
    private override init() { }
    
    static var userModel : User?
    
    static func saveUserModel(user: User)  {
        let userDefaults = UserDefaults.standard
        do {
            try userDefaults.setObject(user, forKey: Constants.userDefaultsKey)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    static func fetchUserModel()  {
        let userDefaults = UserDefaults.standard
        do {
            let model = try userDefaults.getObject(forKey: Constants.userDefaultsKey, castTo: User.self)
            print(model)
            self.userModel = model
        } catch {
            print(error.localizedDescription)
        }
    }
    
    static func removeUserModel() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: Constants.userDefaultsKey)
        userDefaults.synchronize()
    }
}

