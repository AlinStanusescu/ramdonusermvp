//
//  APIClient.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import Foundation

class APIClient: NSObject {
    
    private override init() { }
    
    static func fetchRandomUsers(pageResultstNo: Int,
                                     successHandler: @escaping (User?) -> Void,
                                     errorHandler: @escaping (Error) -> Void) {
        let url =  APIRouter.fetchRandomUsers(pageResultstNo: pageResultstNo).asURLRequest()
        
        URLSession(configuration: .default).userTask(with: url) { users, response, error in
            if let error = error {
                errorHandler(error)
            } else {
                successHandler(users)
            }
        }.resume()
    }
}
