//
//  Constants.swift
//  UserMVP
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import Foundation

struct Constants {
    static let noOfRequestedUsers = 10
    
    static let userDefaultsKey = "userModelResponse"
    struct ProductionServer {
        static let baseURL = "https://randomuser.me"
    }
}
