

Estimate time: 6h
Real time: 
27.10 - 4h
28.10 - 4h

====================
let JSON = """
{
    "results": [{
        "gender": "female",
        "name": {
            "title": "Ms",
            "first": "Ines",
            "last": "Pascual"
        },
        "location": {
            "street": {
                "number": 7861,
                "name": "Calle del Arenal"
            },
            "city": "Guadalajara",
            "state": "Galicia",
            "country": "Spain",
            "postcode": 11414,
            "coordinates": {
                "latitude": "-22.9376",
                "longitude": "118.0868"
            },
            "timezone": {
                "offset": "+9:30",
                "description": "Adelaide, Darwin"
            }
        },
        "email": "ines.pascual@example.com",
        "login": {
            "uuid": "6bbecc5a-0e50-4fae-88eb-7362e95473da",
            "username": "silverladybug295",
            "password": "tank",
            "salt": "4vc8N4rG",
            "md5": "2bf167c4255fc3d6720ae99b3533fc85",
            "sha1": "efead7dbb634285f4b09be0bf019121dcc539ee4",
            "sha256": "1bd0e9f1cf6c8e357648253fbfcdd6f9f6c7d2a99c48a2d1cf4438127b13d2fc"
        },
        "dob": {
            "date": "1967-08-30T09:55:40.474Z",
            "age": 54
        },
        "registered": {
            "date": "2004-11-01T19:46:38.730Z",
            "age": 17
        },
        "phone": "963-341-866",
        "cell": "638-473-516",
        "id": {
            "name": "DNI",
            "value": "93234183-X"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/89.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/89.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/89.jpg"
        },
        "nat": "ES"
    }],
    "info": {
        "seed": "14c97a1fa4e13248",
        "results": 1,
        "page": 1,
        "version": "1.3"
    }
}
"""
